from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.client import Binary
import datetime
import xmlrpc.client as xmlrpclib


url = "http://localhost:8070"
db = "odoo_v12_basic"
username = 'admin'
password = 'admin'
common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, username, password, {})
models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))


class ExampleService:

    def ping(self):
        """Simple function to respond when called
        to demonstrate connectivity.
        """
        return True

    def now(self):
        """Returns the server current date and time."""
        return datetime.datetime.now()

    def show_type(self, arg):
        """Illustrates how types are passed in and out of
        server methods.

        Accepts one argument of any type.

        Returns a tuple with string representation of the value,
        the name of the type, and the value itself.

        """
        return (str(arg), str(type(arg)), arg)

    def raises_exception(self, msg):
        "Always raises a RuntimeError with the message passed in"
        raise RuntimeError(msg)

    def send_back_binary(self, bin):
        """Accepts single Binary argument, and unpacks and
        repacks it to return it."""
        data = bin.data
        print('send_back_binary({!r})'.format(data))
        response = Binary(data)
        return response

    def read(self):
        # Read sale order
        model_name = 'sale.order'
        order_ids = models.execute_kw(db, uid, password, model_name, 'search', [[]])
        order_records = models.execute_kw(db, uid, password, model_name, 'read', [order_ids])
        return order_records

    def create(self, partner_id, candidate_id, line_name, product_id, product_uom):
        # Create Purchase order
        model_name = 'purchase.order'
        vals = {
            'partner_id': partner_id,  # api partner
            'partner_id': partner_id,
            'order_line': [(0, 0, {
                'name': line_name,
                'product_id': product_id,
                'product_qty': 2,
                'product_uom': product_uom,
                'date_planned': self.now(),
                'price_unit': 1000.00,
            })]
        }
        new_id = models.execute_kw(db, uid, password, model_name, 'create', [vals])
        return new_id


if __name__ == '__main__':
    server = SimpleXMLRPCServer(('localhost', 9000), logRequests=True, allow_none=True)
    server.register_introspection_functions()
    server.register_multicall_functions()
    server.register_instance(ExampleService())

    try:
        print('Use Control-C to exit')
        server.serve_forever()
    except KeyboardInterrupt:
        print('Exiting')
