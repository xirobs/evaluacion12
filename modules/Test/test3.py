# -*- coding: utf-8 -*-

import requests
import json

url = 'http://localhost:8069/odoo/create_partner'
headers = {'Content-Type': 'application/json'}

data_res_partner = {
    'params': {
        'name': 'Example',
        'email': 'jamon@test.com',
    }
}

data_json = json.dumps(data_res_partner)
response = requests.post(url=url, data=data_json, headers=headers)
print(response.status_code)
print(response.content)

{
	"jsonrpc": "2.0",
	"service": "object",
	"method": "fields_get",
	"args": [
      "demo_130_1575914447",
      "2",
      "admin",
      "res.partner",
      "read",
      "context"
    ]
}
https://demo3.odoo.com/jsonrpc/

{
      jsonrpc: '2.0',
      method: 'call',
      params: {
        service: 'object',
        method: 'execute_kw',
        args: [
          'Nombre de la base de datos (importa las mayúsculas/minúsculas',
          'id de tu usuario (el admin en Odoo 12 es el 2 por ejemplo)',
          'Contraseña',
          model,
          method,
          [
              'Parametros, en formato lista, Odoo sabe cuales son por el orden en el que estén definidos en el método'
          ],
          context
    ]
      }
    }



{
"params" : {
    "name" : "Order/1/18",
    "session_id" : 1,
    "customer_count" : 2,
    "partner_id" : 9,
    "lines": [
        {
            "product_id": 37,
            "qty" : 2,
            "price_unit" : 2,
            "discount" : 10
        }
        ],
    "pos_reference" : 2,
    "note" : "This is a test note"
}
}

{
  "jsonrpc": "2.0",
  "method": "call",
  "params": {
    "service": "object",
    "method": "execute_kw",
    "args": [
      "odoo_12_demo",
      2,
      "admin",
      "res.partner",
      "search_read",
      [
        []
      ],
      {"domain": ["|", "name", "=", "MyPartner1"], "fields": ["name"]}
    ]
  }
}

{"origin": "A555",
            "client_order_ref": "B555",
            "partner_id": partner_id,  # api partner
            "pricelist_id": 1,  # Public Pricelist (KWD)
            "partner_invoice_id": partner_id,
            "partner_shipping_id": partner_id,
            "order_line": [(0, 0, {
                "name": line_name,
                "product_id": product_id,
                "product_uom_qty": 2,
                "qty_delivered": 2,
                "price_unit": 1000.00,
                "candidate_id": candidate_id,
            })]}

{
  "jsonrpc": "2.0",
  "method": "call",
  "params": {
    "service": "object",
    "method": "execute_kw",
    "args": [
      "odoo_12_demo",
      2,
      "admin",
      "res.partner",
      "search_read",
      [[["is_company", "=", "True"], ["customer", "=", "False"]]],
      {
		"fields": ["name", "country_id", "comment"], "limit": 5
      }
    ]
  }
}

{
    "jsonrpc": "2.0",
    "method": "call",
    "params": {
        "service": "object",
        "method": "execute_kw",
        "args": [
            "odoo_12_demo",
            2,
            "admin",
            "res.partner",
            "create",
            [
                {
                    "name": "Boris Silva"
                }
            ],
            {

            }
        ]
    }
}

{
  "jsonrpc": "2.0",
  "method": "call",
  "params": {
    "service": "object",
    "method": "execute_kw",
    "args": [
      "odoo_12_demo",
      2,
      "admin",
      "sale.order",
      "create",
      [
        {
          "origin": "A555",
          "client_order_ref": "B555",
          "partner_id": 1,
          "pricelist_id": 1,
          "partner_invoice_id": 1,
          "partner_shipping_id": 1,
          "order_line": [
            [
              0,
              0,
              {
                "name": "Prueba 1",
                "product_id": 8,
                "product_uom_qty": 2,
                "qty_delivered": 2,
                "price_unit": 1000
              }
            ]
          ]
        }
      ],
      {}
    ]
  }
}

{
    "jsonrpc": "2.0",
    "method": "call",
    "params": {
        "service": "object",
        "method": "execute_kw",
        "args": [
            "odoo_12_demo",
            2,
            "admin",
            "purchase.order",
            "search_read",
            [

            ],
            {
                "fields": ["name", "envio_pieza", "entrega"], "limit": 5
            }
        ]
    }
}

"taxes_id": [
    [
        0,
        0,
        {
            "name": "Purchase IVA Cobrado (12%)(12.0%)"
        }
    ]
]