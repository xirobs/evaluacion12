# -*- coding: utf-8 -*-
{
    'name': "Laboratoria",

    'summary': """
        Órdenes de Laboratoria""",

    'description': """
        Esta debe crear una orden de compra como un asociado para manejar la compra del producto
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Purchase',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'purchase', 'product', 'sale', 'account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'data/data.xml',
        'views/purchase_views.xml',
        'views/res_partner_views.xml',
        'views/product_views.xml',
    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
    'application': True
}