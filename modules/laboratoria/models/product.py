from odoo import models, fields, api
from odoo.addons import decimal_precision as dp


# class ProductAttribute(models.Model):
#     _inherit = "product.attribute"
#
#     type = fields.Selection([
#         ('radio', 'Radio'),
#         ('select', 'Select'),
#         ('color', 'Color')], default='radio')


class ProductProduct(models.Model):
    _inherit = "product.template"

    normal_price = fields.Float(string='Precio Normal', digits=dp.get_precision('Product Price'))
    flash_price = fields.Float(string='Precio Flash', digits=dp.get_precision('Product Price'))
    info_normal = fields.Text('Descripción', help='Descripción para mostrar las condiciones de tiempo, tecnicas utilizadas o '
                                           'cualquier otro dato relevante al producto.')
    info_flash = fields.Text('Descripción', help='Descripción para mostrar las condiciones de tiempo, tecnicas utilizadas o '
                                           'cualquier otro dato relevante al producto.')

