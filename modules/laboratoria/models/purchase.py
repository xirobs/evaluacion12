from odoo import models, fields, api


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    envio_pieza = fields.Selection([('no', 'No envío pieza'), ('si', 'Si envío pieza')], default='no')
    entrega = fields.Selection([('buscar', 'Ven a buscar'), ('llevar', 'Yo llevo')], default='buscar')
    sale_order_ok = fields.Boolean('Orden de Venta generada', default=False)
    detalle_envio = fields.Text('Detalle de envío', help='Descripción de lo que se está enviando')

    @api.model
    def confirmar_orden(self, vals):
        order_obj = self.env['purchase.order']
        order_id = order_obj.search([('id', '=', vals['id'])])
        res = order_id.button_confirm()
        res = 'La orden de compra posee una orden de venta previa'
        if not order_id.sale_order_ok:
            res = order_id.crear_orden_venta(order_id)
        return res

    def crear_orden_venta(self, order):
        user_id = order.user_id.id
        company_id = order.company_id.id
        partner_id = order.partner_id.id
        self.env['res.users'].search([('id', '=', user_id)]).write({'company_id': company_id})
        order_lst = order.read()[0]
        order_line_lst = order.order_line.read()
        res = self.env['res.users'].search([('id', '=', user_id)]).write({'company_id': partner_id})
        sale_id = self.env['sale.order'].create({
            "origin": order_lst['name'],
            "client_order_ref": order_lst['partner_ref'],
            "partner_id": order_lst['company_id'][0],
            # "pricelist_id": 1,
            "partner_invoice_id": order_lst['company_id'][0],
            "partner_shipping_id": order_lst['company_id'][0],
        })
        for line in order_line_lst:
            self.env['sale.order.line'].create({
                "order_id": sale_id.id,
                "name": line['name'],
                "product_id": line['product_id'][0],
                "product_uom_qty": line['product_qty'],
                "qty_delivered": line['product_qty'],
                "price_unit": line['price_unit'],
                "tax_id": [(6, 0, line['taxes_id'])],
            })
        self.env['res.users'].search([('id', '=', user_id)]).write({'company_id': company_id})
        order.write({'sale_order_ok': True})
        return sale_id.id


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    tipo_precio = fields.Selection([('normal', 'Normal'), ('flash', 'Flash')], required=True)

    # @api.onchange('tipo_precio')
    # def onchange_tipo_precio(self):
    #     if self.tipo_precio == 'normal':
    #         self.price_unit = self.product_id.normal_price
    #     else:
    #         self.price_unit = self.product_id.flash_price
