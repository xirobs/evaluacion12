from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    info = fields.Text('Info', help='Campo info de laboratorio para describir las especialidades del centro y una '
                                    'descripcion general del mismo.')

