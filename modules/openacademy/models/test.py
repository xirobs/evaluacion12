# -*- coding: utf-8 -*-

from datetime import timedelta
from odoo import models, fields, api, exceptions, _


class MainTest(models.Model):
    _name = 'openacademy.test'
    _description = "OpenAcademy Tests"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string="Título", required=True)
    description = fields.Text()
    responsible_id = fields.Many2one('res.users', ondelete='set null', string="Responsible", index=True,
                                     default=lambda self: self.env.user)
    question_ids = fields.One2many('openacademy.question', 'test_id', string="Question")
    session_id = fields.Many2one('openacademy.session', ondelete='cascade', string="Session", required=True)


class Question(models.Model):
    _name = 'openacademy.question'
    _description = "OpenAcademy Tests"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string="Question", required=True)
    description = fields.Text(string="Description")
    responsible_id = fields.Many2one('res.users', ondelete='set null', string="Responsible", index=True,
                                     default=lambda self: self.env.user)
    test_id = fields.Many2one('openacademy.test', ondelete='cascade', string="Test", required=True)
    answer_ids = fields.One2many('openacademy.question_lines', 'question_id', string="Answers")


class QuestionLines(models.Model):
    _name = 'openacademy.question_lines'
    _description = "OpenAcademy Tests Lines"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    order = fields.Char(string="Order", required=True)
    answers = fields.Char(string="Answers", required=True)
    question_id = fields.Many2one('openacademy.question', ondelete='cascade', string="Question", required=True)